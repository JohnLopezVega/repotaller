const URL_NORTHWIND = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";

var productosObtenidos;

function getProductos() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //console.log(request.responseText);
            productosObtenidos = JSON.parse(request.responseText);
            procesarProductos();
        }
    }
    request.open("GET", URL_NORTHWIND, true);
    request.send();
}

function procesarProductos() {
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for (let index = 0; index < productosObtenidos.value.length; index++) {
        //console.log(productosObtenidos.value[index]);
        var nuevaFila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = productosObtenidos.value[index].ProductName;

        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText = productosObtenidos.value[index].UnitPrice;

        var columnaStock = document.createElement("td");
        columnaStock.innerText = productosObtenidos.value[index].UnitsInStock;

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);

        tbody.appendChild(nuevaFila);
    }

    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}