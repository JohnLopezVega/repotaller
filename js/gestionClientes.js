const URL_CUSTOMERS = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";

var clientesObtenidos;

function getClientes() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //console.log(request.responseText);
            clientesObtenidos = JSON.parse(request.responseText);
            procesarClientes();
        }
    }
    request.open("GET", URL_CUSTOMERS, true);
    request.send();
}

function procesarClientes() {
    var divTabla = document.getElementById("divTablaClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for (let index = 0; index < clientesObtenidos.value.length; index++) {
        var nuevaFila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = clientesObtenidos.value[index].ContactName;

        var columnaEmpresa = document.createElement("td");
        columnaEmpresa.innerText = clientesObtenidos.value[index].City;

        var columnaBandera = document.createElement("td");
        var imagen = document.createElement("img");
        imagen.classList.add("flag");
        if (clientesObtenidos.value[index].Country == "UK") {
            clientesObtenidos.value[index].Country = "United-Kingdom";
        }
        imagen.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + clientesObtenidos.value[index].Country + ".png";
        columnaBandera.appendChild(imagen);

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaEmpresa);
        nuevaFila.appendChild(columnaBandera);

        tbody.appendChild(nuevaFila);
    }

    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}